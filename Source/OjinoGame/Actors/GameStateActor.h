
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameStateActor.generated.h"

UCLASS()
class OJINOGAME_API AGameStateActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AGameStateActor();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

};
