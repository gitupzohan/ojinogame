
#include "FinishActor.h"

#include "OjinoGameCharacter.h"
#include "Components/BoxComponent.h"

AFinishActor::AFinishActor()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	SetRootComponent(Box);
	Box->OnComponentBeginOverlap.AddDynamic(this, &AFinishActor::BeginOverlapBox);
}

void AFinishActor::BeginOverlapBox(UPrimitiveComponent* PrimitiveComponent, AActor* Actor,
	UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg, const FHitResult& HitResult)
{
	if (HasAuthority() && Cast<AOjinoGameCharacter>(Actor))
	{
		OnPlayerFinished.Broadcast();
	}
}

