
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FinishActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerFinished);

UCLASS()
class OJINOGAME_API AFinishActor : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AFinishActor();

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite)
	class UBoxComponent* Box;

	FOnPlayerFinished OnPlayerFinished;
	
	UFUNCTION()
	void BeginOverlapBox(UPrimitiveComponent* PrimitiveComponent, AActor* Actor, UPrimitiveComponent* PrimitiveComponent1, int I, bool bArg, const FHitResult& HitResult);
};

