#include "Doll.h"

#include "OjinoGameState.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

ADoll::ADoll()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	Body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	Body->SetupAttachment(RootComponent);

	Eyes = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Eyes"));
	Eyes->SetupAttachment(Body);
}

void ADoll::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void ADoll::Rotation()
{
	const FRotator Rot = GetActorRotation();
	const float Yaw = DirectionRotate * 0.01f * RotationRate + Rot.Yaw;
	SetActorRelativeRotation(FRotator(0.f, Yaw, 0.f));
	if (FMath::IsNearlyEqual(Yaw, DesiredRotation, RotationRate * 0.01f) || FMath::IsNearlyEqual(Yaw, DesiredRotation * -1.f, RotationRate * 0.01f))
	{
		SetActorRelativeRotation(FRotator(0.f, DesiredRotation, 0.f));
		GetWorldTimerManager().ClearTimer(RotationTimer);
		if (GameState->State != EGameState::EndGame)
		{
			const float Delay = bIsRed ? FMath::RandRange(3.f, 6.f) : FMath::RandRange(3.f, 5.f);
			GetWorldTimerManager().SetTimer(BeginRotationTimer, this, &ADoll::BeginRotation, Delay, false);
			if (GetActorRotation().Yaw != 0.f)
			{
				GameState->ChangeState_Multicast(EGameState::Red);
			}
		}
	}
}

void ADoll::PlaySound_Multicast_Implementation()
{
	UGameplayStatics::PlaySound2D(GetWorld(), Signal);
}

void ADoll::ChangeColorEyes_Multicast_Implementation(FLinearColor Color)
{
	DynamicMaterial->SetVectorParameterValue("Color", Color);
	OnChangeColor.Broadcast(Color);
}

void ADoll::StartPlay()
{
	GetWorldTimerManager().SetTimer(BeginRotationTimer,this,&ADoll::BeginRotation,FMath::RandRange(2.f,4.f),false);
}

void ADoll::BeginRotation()
{
	RotationRate = FMath::RandRange(RotationRateMin, RotationRateMax);
	DirectionRotate = FMath::RandBool() ? 1.f : -1.f;
	DesiredRotation = GetActorRotation().Yaw == 0.f ? 180.f : 0.f;
	bIsRed = GetActorRotation().Yaw == 0.f ? true : false;
	GetWorldTimerManager().SetTimer(RotationTimer, this, &ADoll::Rotation, 0.01f, true);
	if (GameState)
	{
		if (GetActorRotation().Yaw == 0.f)
		{
			ChangeColorEyes_Multicast(FLinearColor::Red);
		}
		else
		{
			GameState->ChangeState_Multicast(EGameState::Green);
			ChangeColorEyes_Multicast(FLinearColor::Green);
		}
	}
	PlaySound_Multicast();
}

void ADoll::ChangeGameState(EGameState State)
{
	if (State == EGameState::EndGame)
		GetWorldTimerManager().ClearTimer(BeginRotationTimer);
}

void ADoll::BeginPlay()
{
	Super::BeginPlay();
	GameState = GetWorld()->GetGameState<AOjinoGameState>();
	DynamicMaterial = Eyes->CreateDynamicMaterialInstance(0, Eyes->GetMaterial(0));
	if (GameState && HasAuthority())
	{
		GameState->OnStartPlay.AddDynamic(this, &ADoll::StartPlay);
		GameState->OnChangeState.AddDynamic(this, &ADoll::ChangeGameState);
	}
}
