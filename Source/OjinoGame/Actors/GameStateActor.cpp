
#include "GameStateActor.h"

AGameStateActor::AGameStateActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AGameStateActor::BeginPlay()
{
	Super::BeginPlay();
}

void AGameStateActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

