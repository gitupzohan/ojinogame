
#include "LightActor.h"

#include "Doll.h"
#include "EngineUtils.h"
#include "OjinoGameState.h"
#include "Components/PointLightComponent.h"

ALightActor::ALightActor()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SetRootComponent(StaticMesh);

	PointLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("PointLight"));
	PointLight->SetupAttachment(StaticMesh);
}

void ALightActor::BeginPlay()
{
	Super::BeginPlay();
	DynamicMaterial = StaticMesh->CreateDynamicMaterialInstance(0, StaticMesh->GetMaterial(0));
	auto GameState = GetWorld()->GetGameState<AOjinoGameState>();
	if (GameState && HasAuthority())
	{
		GameState->OnChangeState.AddDynamic(this, &ALightActor::ChangeGameState);
		
		for (TActorIterator<ADoll> It(GetWorld()); It; ++It)
		{
			ADoll* Doll = *It;
			if (Doll)
			{
				Doll->OnChangeColor.AddDynamic(this, &ALightActor::ChangeColor_Multicast);
			}
		}
	}
}

void ALightActor::ChangeGameState(EGameState State)
{
	switch (State)
	{
		case EGameState::WaitStart:
			ChangeColor_Multicast(FLinearColor::Yellow);
			break;
		case EGameState::Start:
			ChangeColor_Multicast(FLinearColor::Green);
			break;
		case EGameState::Red:
			ChangeColor_Multicast(FLinearColor::Red);
			break;
		case EGameState::Green:
			break;
		case EGameState::EndGame:
			ChangeColor_Multicast(FLinearColor::Blue);
			break;
		default: ;
	}
}

void ALightActor::ChangeColor_Multicast_Implementation(FLinearColor Color)
{
	DynamicMaterial->SetVectorParameterValue("Color", Color);
	PointLight->SetLightColor(Color);
}


