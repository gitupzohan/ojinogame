// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OjinoGameState.h"
#include "GameFramework/Actor.h"
#include "LightActor.generated.h"

UCLASS()
class OJINOGAME_API ALightActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALightActor();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UPointLightComponent* PointLight;

	UPROPERTY()
	UMaterialInstanceDynamic* DynamicMaterial = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UFUNCTION(NetMulticast, Reliable)
	void ChangeColor_Multicast(FLinearColor Color);
	UFUNCTION()
	void ChangeGameState(EGameState State);
};
