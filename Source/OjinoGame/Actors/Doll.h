#pragma once

#include "CoreMinimal.h"
#include "OjinoGameState.h"
#include "GameFramework/Actor.h"
#include "Doll.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeColor, FLinearColor, Color);

UCLASS()
class OJINOGAME_API ADoll : public AActor
{
	GENERATED_BODY()

public:
	ADoll();
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* Eyes;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* Body;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float RotationRateMin = 60.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float RotationRateMax = 180.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float RedDelayMin = 3.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float RedDelayMax = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float GreenDelayMin = 3.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Movement")
	float GreenDelayMax = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="SFX")
	USoundBase* Signal = nullptr;

	FOnChangeColor OnChangeColor;
	
private:
	
	float RotationRate = 180.f;
	
	bool bIsRed = false;
	
	float DirectionRotate = 1.0;
	
	float DesiredRotation = 0.f;
	
	FTimerHandle RotationTimer;

	FTimerHandle BeginRotationTimer;
	
	UPROPERTY()
	AOjinoGameState* GameState = nullptr;
	
	UPROPERTY()
	UMaterialInstanceDynamic* DynamicMaterial = nullptr;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	
	void Rotation();
	
	UFUNCTION()
	void StartPlay();

	UFUNCTION(Reliable, NetMulticast)
	void PlaySound_Multicast();
	
	UFUNCTION(Reliable, NetMulticast)
	void ChangeColorEyes_Multicast(FLinearColor Color);
	
	void BeginRotation();

	UFUNCTION()
	void ChangeGameState(EGameState State);
	
	virtual void BeginPlay() override;
	
};
