
using UnrealBuildTool;

public class OjinoGame : ModuleRules
{
	public OjinoGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"HeadMountedDisplay", 
			"EnhancedInput" ,
			"UMG"
		});
		
		PublicIncludePaths.AddRange(new string[]
		{
			"OjinoGame/Actors",
			"OjinoGame/Character",
			"OjinoGame/Game"
		});
	}
	
	
}
