#include "OjinoGameState.h"

#include "EngineUtils.h"
#include "FinishActor.h"
#include "OjinoGameCharacter.h"
#include "OjinoGameGameMode.h"
#include "OjinoPlayerController.h"
#include "OjinoPlayerState.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"


AOjinoGameState::AOjinoGameState()
{
	State = EGameState::WaitStart;
}

void AOjinoGameState::BeginStart_Multicast_Implementation()
{
	OnStartPlay.Broadcast();
}

void AOjinoGameState::CreateMatchResultWidget()
{
	for (auto PS : PlayerArray)
	{
		AOjinoPlayerController* Controller = Cast<AOjinoPlayerController>(PS->GetPlayerController());
		if (Controller)
		{
			Controller->CreateMatchResultWidget_Client();
		}
	}
}

void AOjinoGameState::FillNonFinishedPlayers_Multicast_Implementation()
{
	for (auto PS : PlayerArray)
	{
		NonFinishedPlayers.Add(Cast<AOjinoPlayerState>(PS));
		if (HasAuthority() && PS->GetPawn())
		{
			PS->GetPawn()->OnDestroyed.AddDynamic(this, &AOjinoGameState::PlayerDestroyed);
		}
	}
}

void AOjinoGameState::SortPlayersByRecordTime()
{
	float BestTime = 0.f;
	AOjinoPlayerState* BestPS = nullptr;
	for (AOjinoPlayerState* PS : NonFinishedPlayers)
	{
		if (PS && PS->RecordTime > BestTime)
		{
			BestTime = PS->RecordTime;
			BestPS = PS;
		}
	}
	if (NonFinishedPlayers.Num() > 0 && BestPS)
	{
		FinishedPlayers.Add(BestPS);
		NonFinishedPlayers.Remove(BestPS);
		SortPlayersByRecordTime();
	}
	else
	{
		for (AOjinoPlayerState* PS : FinishedPlayers)
		{
			FPlayerInfo PlayerInfo;
			PlayerInfo.Number = FinishedPlayers.IndexOfByKey(PS);
			PlayerInfo.PlayerName = PS->GetPlayerName();
			PlayerInfo.RecordTime = PS->RecordTime;
			PlayersInfo.Add(PlayerInfo);
		}
	}
}

void AOjinoGameState::PlayerDestroyed(AActor* Actor)
{
	for (auto PS : NonFinishedPlayers)
	{
		if (PS->GetPawn<AOjinoGameCharacter>() != nullptr)
		{
			return;
		}
	}
	if (HasAuthority())
	{
		for (auto PS : FinishedPlayers)
		{
			PS->StopRecordTimer(nullptr);
		}
	}
	SortPlayersByRecordTime();
	ChangeState_Multicast(EGameState::EndGame);
}

void AOjinoGameState::PlayerFinished()
{
	PlayerDestroyed(nullptr);
}

void AOjinoGameState::OnStartingNewPlayer()
{
	if (PlayerArray.Num() == NumPlayersToStart)
	{
		GetWorldTimerManager().ClearTimer(UpdateTimer);
		if (HasAuthority())
		{
			GetWorldTimerManager().SetTimer(UpdateTimer, this, &ThisClass::UpdateTimeToStart, 0.1f, true);
			FTimerHandle Timer;
			GetWorldTimerManager().SetTimer(Timer, this, &ThisClass::FillNonFinishedPlayers_Multicast, 1.f, false);
			ChangeState_Multicast(EGameState::WaitStart);
		}
	}
}

void AOjinoGameState::DestroyAllNonFinishedPlayer_Multicast_Implementation()
{
	for (const auto PS : NonFinishedPlayers)
	{
		AOjinoGameCharacter* PlayerChar = PS->GetPawn<AOjinoGameCharacter>();
		if (PlayerChar && !PlayerChar->bIsFinished)
		{
			PlayerChar->OnDestroyed.RemoveDynamic(this, &AOjinoGameState::PlayerDestroyed);
			PlayerChar->Death_OnServer_Implementation();
		}
	}
	if (HasAuthority())
	{
		for (auto PS : FinishedPlayers)
		{
			PS->StopRecordTimer(nullptr);
		}
	}
}

void AOjinoGameState::ChangeState_Multicast_Implementation(EGameState Stat)
{
	if (State == EGameState::EndGame)
		return;
	
	State = Stat;
	OnChangeState.Broadcast(Stat);
}

void AOjinoGameState::ChangeState_OnServer_Implementation(EGameState Stat)
{
	ChangeState_Multicast(Stat);
}

void AOjinoGameState::RunTimerUpdateTime_OnServer_Implementation()
{
	GetWorldTimerManager().SetTimer(UpdateTimer, this, &ThisClass::UpdateTime, 1.f, true);
}

void AOjinoGameState::UpdateTimeWaitPlayers()
{
	if (TimeWaitPlayers > 0.f)
	{
		TimeWaitPlayers -= 0.1f;
	}
	else
	{
		GetWorldTimerManager().ClearTimer(UpdateTimer);
		if (HasAuthority())
		{
			GetWorldTimerManager().SetTimer(UpdateTimer, this, &ThisClass::UpdateTimeToStart, 0.1f, true);
			FTimerHandle Timer;
			GetWorldTimerManager().SetTimer(Timer, this, &ThisClass::FillNonFinishedPlayers_Multicast, 1.f, false);
			ChangeState_Multicast(EGameState::WaitStart);
		}
	}
}
void AOjinoGameState::UpdateTimeToStart()
{
	if (TimeToStart > 0.f)
	{
		TimeToStart -= 0.1f;
	}
	else
	{
		GetWorldTimerManager().ClearTimer(UpdateTimer);
		if (HasAuthority())
		{
			GetWorldTimerManager().SetTimer(UpdateTimer, this, &ThisClass::UpdateTime, 1.f, true);
			BeginStart_Multicast();
			ChangeState_Multicast(EGameState::Start);
		}
	}
}

void AOjinoGameState::UpdateTime()
{
	if (Time > 0.f)
	{
		Time -= 1.f;
		PlayTickSound_Multicast();
	}
	else
	{
		GetWorldTimerManager().ClearTimer(UpdateTimer);
		DestroyAllNonFinishedPlayer_Multicast();
		SortPlayersByRecordTime();
		ChangeState_Multicast(EGameState::EndGame);
	}
}

void AOjinoGameState::PlayTickSound_Multicast_Implementation()
{
	UGameplayStatics::PlaySound2D(GetWorld(), TickSound);
}

void AOjinoGameState::BeginPlay()
{
	Super::BeginPlay();
	if (HasAuthority())
	{
		AOjinoGameGameMode* GM = GetWorld()->GetAuthGameMode<AOjinoGameGameMode>();
		if (GM)
		{
			GM->OnStartingNewPlayer.AddDynamic(this,&AOjinoGameState::OnStartingNewPlayer);
		}
		GetWorldTimerManager().SetTimer(UpdateTimer, this, &ThisClass::UpdateTimeWaitPlayers, 0.1f, true);
		for (TActorIterator<AFinishActor> It(GetWorld()); It; ++It)
		{
			AFinishActor* FinishActor = *It;
			if (FinishActor)
			{
				FinishActor->OnPlayerFinished.AddDynamic(this, &AOjinoGameState::PlayerFinished);
			}
		}
	}
}

void AOjinoGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOjinoGameState, State);
	DOREPLIFETIME(AOjinoGameState, Time);
	DOREPLIFETIME(AOjinoGameState, TimeToStart);
	DOREPLIFETIME(AOjinoGameState, TimeWaitPlayers);
	DOREPLIFETIME(AOjinoGameState, PlayersInfo);
}
