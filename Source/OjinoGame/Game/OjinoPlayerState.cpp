#include "OjinoPlayerState.h"

#include "OjinoGameState.h"
#include "Net/UnrealNetwork.h"

AOjinoPlayerState::AOjinoPlayerState()
{
	OnPawnSet.AddUniqueDynamic(this, &AOjinoPlayerState::PawnSet);
}

void AOjinoPlayerState::StopRecordTimer(AActor* Actor)
{
	GetWorldTimerManager().ClearTimer(RecordTimer);
}

void AOjinoPlayerState::BeginPlay()
{
	Super::BeginPlay();
	if (HasAuthority())
	{
		AOjinoGameState* GS = GetWorld()->GetGameState<AOjinoGameState>();
		if (GS)
		{
			GS->OnStartPlay.AddDynamic(this, &AOjinoPlayerState::RunUpdateRecordTimer);
		}
	}
}

void AOjinoPlayerState::RunUpdateRecordTimer()
{
	GetWorldTimerManager().SetTimer(RecordTimer, this, &AOjinoPlayerState::UpdateRecordTime, 0.1f, true);
}

void AOjinoPlayerState::UpdateRecordTime()
{
	RecordTime += 0.1f;
}

void AOjinoPlayerState::PawnSet(APlayerState* Player, APawn* NewPawn, APawn* OldPawn)
{
	if (NewPawn)
		NewPawn->OnDestroyed.AddDynamic(this, &AOjinoPlayerState::StopRecordTimer);
	if (!bIsRename && HasAuthority())
	{
		bIsRename = true;
		SetNewName();
	}
}

void AOjinoPlayerState::SetNewName()
{
	int i = FMath::RandRange(0,9);
	int j = FMath::RandRange(0,9);
	SetPlayerName(FString::Printf(TEXT("Player %s"),*FString::FromInt(i+j)));
}

void AOjinoPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOjinoPlayerState, RecordTime);
}
