#include "OjinoPlayerController.h"

#include "EngineUtils.h"
#include "OjinoGameCharacter.h"
#include "OjinoSpectatorPawn.h"
#include "Blueprint/UserWidget.h"

AOjinoPlayerController::AOjinoPlayerController()
{
	bReplicates = true;
}

void AOjinoPlayerController::DeathPawn_OnServer_Implementation(AActor* Actor)
{
	DeathPawn(Actor);
}

void AOjinoPlayerController::ChangeAttachedActor_Server_Implementation()
{
	if (AttachedActor)
		AttachedActor->OnDestroyed.RemoveDynamic(this, &AOjinoPlayerController::DeathPawn_OnServer);
	AttachedActors.Empty();
	for (TActorIterator<AOjinoGameCharacter> It(GetWorld()); It; ++It)
	{
		AOjinoGameCharacter* OjinoCharacter = *It;
		if (OjinoCharacter)
		{
			AttachedActors.Add(OjinoCharacter);
		}
	}
	CurrenIndexActor++;
	if (CurrenIndexActor > AttachedActors.Num() - 1)
		CurrenIndexActor = 0;
	if (AttachedActors.IsValidIndex(CurrenIndexActor))
	{
		AttachedActor = AttachedActors[CurrenIndexActor];
		GetPawn()->AttachToActor(AttachedActor, FAttachmentTransformRules::SnapToTargetIncludingScale);
	}
}

void AOjinoPlayerController::DeathPawn(AActor* Actor)
{
	auto Spect = GetWorld()->SpawnActor<AOjinoSpectatorPawn>(SpectatorClass, GetSpawnLocation(), FRotator(0.f));
	Spect->AddActorLocalRotation(FRotator(0.f,90.f,0.f));
	Possess(Spect);
}

void AOjinoPlayerController::ChangeGameState(EGameState State)
{
	if (State == EGameState::WaitStart)
	{
		FTimerHandle Timer;
		GetWorldTimerManager().SetTimer(Timer ,this, &AOjinoPlayerController::CreateTimerToStartWidget_Client, 1.f, false);
	}
	if (State == EGameState::EndGame)
	{
		FTimerHandle Timer;
		GetWorldTimerManager().SetTimer(Timer ,this, &AOjinoPlayerController::CreateMatchResultWidget_Client, 1.f, false);
	}
}

void AOjinoPlayerController::BeginPlay()
{
	Super::BeginPlay();
	AOjinoGameCharacter* MyPawn = GetPawn<AOjinoGameCharacter>();
	if (MyPawn)
	{
		MyPawn->OnDestroyed.AddDynamic(this, &AOjinoPlayerController::DeathPawn_OnServer);
	}
	AOjinoGameState* GS = GetWorld()->GetGameState<AOjinoGameState>();
	if (GS)
	{
		GS->OnChangeState.AddDynamic(this,&AOjinoPlayerController::ChangeGameState);
	}
}

void AOjinoPlayerController::CreateTimerToStartWidget_Client_Implementation()
{
	TimerToStartWidget = CreateWidget(this, TimerToStartWidgetClass);
	if (TimerToStartWidget)
	{
		TimerToStartWidget->AddToViewport();
	}
}

void AOjinoPlayerController::CreateMatchResultWidget_Client_Implementation()
{
	MatchResultWidget = CreateWidget(this, MatchResultWidgetClass);
	if (MatchResultWidget)
	{
		MatchResultWidget->AddToViewport();
	}
}
