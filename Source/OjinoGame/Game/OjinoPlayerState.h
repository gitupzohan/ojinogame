#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "OjinoPlayerState.generated.h"

UCLASS()
class OJINOGAME_API AOjinoPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	AOjinoPlayerState();
	
	UPROPERTY(BlueprintReadOnly, Replicated)
	float RecordTime = 0.f;

	FTimerHandle RecordTimer;
	
	bool bIsRename = false;
	
	UFUNCTION()
	void StopRecordTimer(AActor* Actor);
	
	virtual void BeginPlay() override;
	UFUNCTION()
	void RunUpdateRecordTimer();
	void UpdateRecordTime();

	UFUNCTION()
	void PawnSet(APlayerState* Player, APawn* NewPawn, APawn* OldPawn);

	void SetNewName();
	
protected:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
