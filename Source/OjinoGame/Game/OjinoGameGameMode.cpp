
#include "OjinoGameGameMode.h"
#include "OjinoGameCharacter.h"

AOjinoGameGameMode::AOjinoGameGameMode()
{
	DefaultPawnClass = AOjinoGameCharacter::StaticClass();
}

void AOjinoGameGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);
	OnStartingNewPlayer.Broadcast();
}
