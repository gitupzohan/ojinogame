#pragma once

#include "CoreMinimal.h"
#include "OjinoGameState.h"
#include "GameFramework/PlayerController.h"
#include "OjinoPlayerController.generated.h"

class AOjinoSpectatorPawn;
UCLASS()
class OJINOGAME_API AOjinoPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AOjinoPlayerController();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> TimerToStartWidgetClass;
	
	UPROPERTY()
	UUserWidget* TimerToStartWidget = nullptr;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> MatchResultWidgetClass;
	
	UPROPERTY()
	UUserWidget* MatchResultWidget = nullptr;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AOjinoSpectatorPawn> SpectatorClass;
	
	UPROPERTY()
	AActor* AttachedActor = nullptr;

	int32 CurrenIndexActor = 0;

	UPROPERTY()
	TArray<AActor*> AttachedActors;

	UFUNCTION(Reliable, Client)
	void CreateTimerToStartWidget_Client();
	UFUNCTION(Reliable, Client)
	void CreateMatchResultWidget_Client();
	UFUNCTION(Reliable, Server)
	void DeathPawn_OnServer(AActor* Actor);
	UFUNCTION(Reliable, Server)
	void ChangeAttachedActor_Server();
	UFUNCTION()
	void DeathPawn(AActor* Actor);
	
	UFUNCTION()
	void ChangeGameState(EGameState State);
	
	virtual void BeginPlay() override;
};
