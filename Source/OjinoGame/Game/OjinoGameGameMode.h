#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OjinoGameGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartingNewPlayer);

UCLASS(minimalapi)
class AOjinoGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOjinoGameGameMode();

	FOnStartingNewPlayer OnStartingNewPlayer;

	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;
};
