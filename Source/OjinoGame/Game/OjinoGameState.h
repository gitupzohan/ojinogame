#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "OjinoGameState.generated.h"

class AOjinoPlayerState;

UENUM(BlueprintType)
enum class EGameState : uint8
{
	WaitStart UMETA(DisplayName = "WaitStart"),
	Start UMETA(DisplayName = "Start"),
	Red UMETA(DisplayName = "Red"),
	Green UMETA(DisplayName = "Green"),
	EndGame UMETA(DisplayName = "EndGame"),
};

USTRUCT(BlueprintType)
struct FPlayerInfo
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadOnly)
	int Number = 0;
	
	UPROPERTY(BlueprintReadOnly)
	FString PlayerName;
	
	UPROPERTY(BlueprintReadOnly)
	float RecordTime = 0.f;
	
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeState, EGameState, State);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartPlay);

UCLASS()
class OJINOGAME_API AOjinoGameState : public AGameStateBase
{
	GENERATED_BODY()
public:
	AOjinoGameState();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated)
	float Time = 30.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated)
	float TimeToStart = 5.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated)
	float TimeWaitPlayers = 5.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int NumPlayersToStart = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="SFX")
	USoundBase* TickSound = nullptr;

	FTimerHandle UpdateTimer;
	
	UPROPERTY(BlueprintReadOnly, Replicated)
	TArray<FPlayerInfo> PlayersInfo;

	UPROPERTY(BlueprintReadWrite, Replicated)
	EGameState State;

	UPROPERTY(BlueprintAssignable)
	FOnChangeState OnChangeState;
	UPROPERTY(BlueprintAssignable)
	FOnStartPlay OnStartPlay;
	
	UPROPERTY(BlueprintReadWrite)
	TArray<AOjinoPlayerState*> FinishedPlayers;
	UPROPERTY(BlueprintReadWrite)
	TArray<AOjinoPlayerState*> NonFinishedPlayers;

	UFUNCTION(Reliable, Server)
	void ChangeState_OnServer(EGameState Stat);
	UFUNCTION(Reliable, NetMulticast)
	void ChangeState_Multicast(EGameState Stat);
	UFUNCTION(Reliable, Server)
	void RunTimerUpdateTime_OnServer();
	void UpdateTime();
	void UpdateTimeToStart();
	void UpdateTimeWaitPlayers();

	UFUNCTION(Reliable, NetMulticast)
	void BeginStart_Multicast();

	void CreateMatchResultWidget();
	
	UFUNCTION(Reliable, NetMulticast)
	void FillNonFinishedPlayers_Multicast();
	
	void SortPlayersByRecordTime();
	
	UFUNCTION()
	void PlayerDestroyed(AActor* Actor);
	UFUNCTION()
	void PlayerFinished();

protected:
	UFUNCTION()
	void OnStartingNewPlayer();
	
	UFUNCTION(Reliable, NetMulticast)
	void DestroyAllNonFinishedPlayer_Multicast();
	
	UFUNCTION(Reliable, NetMulticast)
	void PlayTickSound_Multicast();
	
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
